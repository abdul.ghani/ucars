from django.core.management.base import BaseCommand, CommandError
from ucars.ucarapp.models import Car, Pricing, Attribute
import json
class Command(BaseCommand):
    help = 'Used for loading data from json into models.'

    def handle(self,*args,**kwargs):
        """ Loads data from ford.json into the database.
            Custom management command to read from json into django models.

        :param args:
        :param kwargs:
        :return:
        """
        with open("dump.json") as f:
            content = json.load(f)
        for x in content["Response"]["Model"]:
            car = Car()
            car.ModelName = x["ModelName"]
            car.Make = x["Make"]
            car.VehicleType = x["VehicleType"]
            car.Year = x["Year"]
            car.Href = x["href"]
            car.save()
            car_dict = {}
            car_dict["High"] = x["Pricing"]["High"]
            Pricing.objects.create(car=car, Range="High", BaseXPlan=car_dict["High"]["BaseXPlan"], BaseAZPlan=car_dict["High"]["BaseAZPlan"], BaseMSRP=car_dict["High"]["BaseMSRP"])
            car_dict["Low"] = x["Pricing"]["Low"]
            Pricing.objects.create(car=car, Range="Low", BaseXPlan=car_dict["Low"]["BaseXPlan"], BaseAZPlan=car_dict["Low"]["BaseAZPlan"], BaseMSRP=car_dict["Low"]["BaseMSRP"])
            if type(x.get("Attribute")) is list:
                for attribute in x.get("Attribute"):
                    key1 = attribute["name"]
                    value1 = attribute.get("Value")
                    Attribute.objects.create(CarObj=car, Key=key1, Value=value1)
            else:
                key1 = x.get("Attribute").get("name")
                value1 = x.get("Attribute").get("Value")
                Attribute.objects.create(CarObj=car, Key=key1, Value=value1)









