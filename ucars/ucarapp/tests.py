from django.test import TestCase
from .models import Car, FuelEfficiency, Pricing, SeatingCapacity
from django.core.management import call_command
import json
class CarTestCase(TestCase):
    """Tests: First tests homepage view;
        then tests if multiple objects of models result in a plural name in Django admin.

    """

# Create your tests here.
    def test_view(self):
        resp = self.client.get('/')
        self.assertEqual(resp.status_code, 200)

    def test_data_insertion(self):
        call_command("Loadjson")
        count_db = len(Car.objects.all())
        print(count_db)
        with open("dump.json") as f:
            json_content = json.load(f)
        count_json = len(json_content["Response"]["Model"])
        print(count_json)
        self.assertEqual(count_db, count_json)


   # def test_attribute_choice(self):
    #    obj=Car.objects.create(Make="Ford",ModelName="Explorer", VehicleType="suv", Year="1999")
   #     self.assertIn(obj.Make, Car.MAKE_CHOICES)

    def test_verbose_name_plural(self):
        self.assertEqual(str(Car._meta.verbose_name_plural), "cars")
        self.assertEqual(str(FuelEfficiency._meta.verbose_name_plural), "fuel efficiencies")
        self.assertEqual(str(Pricing._meta.verbose_name_plural), "pricings")
        self.assertEqual(str(SeatingCapacity._meta.verbose_name_plural), "seating capacities")









