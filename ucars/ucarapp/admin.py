from django.contrib import admin
from .models import Car,Pricing,Attribute,Favorites
# Register your models here.

admin.site.register(Car)
admin.site.register(Pricing)
admin.site.register(Attribute)
admin.site.register(Favorites)

