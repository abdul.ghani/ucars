from django.shortcuts import render
from django.contrib.auth.models import User
from .models import Car,Pricing,Favorites, Attribute
from django.utils.timezone import localtime

from django.http import HttpResponse, HttpResponseRedirect
from django.views import View
from django.views.decorators.http import require_http_methods
from django import forms
from django.shortcuts import render, get_object_or_404
from django.contrib.auth import login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.forms import UserCreationForm
from django.contrib.contenttypes.models import ContentType

from django.db.models import F
from django.forms.models import model_to_dict
from django.urls import reverse
from django.utils.decorators import method_decorator


def obj_price_img_list(cars):
    """ Take a list of Car objects and Pricing objects and return a zipped list;
        which contains the car object, the pricing , and the URL for thumbnail image of the car.
        If image is not available in DB, give a default URL.
    :param cars: List of cars to be displayed.
    :param prices: Corresponding pricing objects for above cars.
    :return:
    """
    base_url = "http://assets.forddirect.fordvehicles.com/assets/"
    image_url = []
    prices = Pricing.objects.filter(Range = "Low")
    for car in cars:
        flag = False
        attribute_objects = Attribute.objects.filter(CarObj=car)
        for attribute_obj in attribute_objects:
            if (attribute_obj.Key == "HomePage_Vehicle_Image"):
                image_url.append(base_url + attribute_obj.Value)
                flag = True
        if flag == False:
            # If an image URL doesn't exist for the Car, use a default.
            image_url.append("http://assets.forddirect.fordvehicles.com/assets/NGBS/CPO/CPO_8801EF28-0A2F-1085-7806-9B1078069B10.jpg")
    cars_prices_url = zip(cars, prices, image_url)
    return cars_prices_url

def obj_img_list(cars):
    """Does the same as above function, just for cars and image URLs.

    :param cars: List of cars.
    :return:
    """
    base_url = "http://assets.forddirect.fordvehicles.com/assets/"
    image_url = []
    for car in cars:
        flag = False
        attribute_objects = Attribute.objects.filter(CarObj=car)
        for attribute_obj in attribute_objects:
            if (attribute_obj.Key == "HomePage_Vehicle_Image"):
                image_url.append(base_url + attribute_obj.Value)
                flag = True
        if flag == False:
            image_url.append(
                "http://assets.forddirect.fordvehicles.com/assets/NGBS/CPO/CPO_8801EF28-0A2F-1085-7806-9B1078069B10.jpg")
    cars_url = zip(cars, image_url)
    return cars_url

def homepage(request):
    """
    Render a homepage with all cars for now.
    """
    user = None
    if request.user.is_authenticated():
        user = request.user
    try:
        car_id = request.POST['add_to_fav']
        Favorites.objects.create(User_fav=user,Car_fav= Car.objects.get(id=car_id))
    except:
        pass
    cars = Car.objects.all()
    cars_prices_url = obj_price_img_list(cars)
    return render(request,"home.html",{"cars_prices_url":cars_prices_url})


@login_required
def imgview(request):
    """Test the display with image functionality

    :param request:
    :return:
    """
    user = None
    if request.user.is_authenticated():
        user = request.user
    cars = Car.objects.all()
    #price_obj = Pricing.objects.get(Range="Low")
    prices = []
    prices = Pricing.objects.filter(Range="Low")
    cars_prices_url = obj_price_img_list(cars)
    return render(request,"imgview.html",{"cars_prices_url":cars_prices_url})

@login_required
def favorites(request):
    fav_objects = Favorites.objects.select_related('Car_fav').filter(User_fav=request.user)
    #time_of_creation = []
    car_id_list = []
    for car_obj in fav_objects:
        car_id_list.append(car_obj.Car_fav)
        #time_of_creation.append(car_obj.created_at)
    cars_prices_url = obj_price_img_list(car_id_list)
    #cars_prices_url_time = zip(cars_prices_url,time_of_creation)
    return render(request, "favorites.html", {"fav_obj":fav_objects,"cars_prices_url": cars_prices_url})



class MyUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        fields = UserCreationForm.Meta.fields

class CreateProfileView(View):
    """Sign up functionality.

    """
    def post(self, request):
        form = MyUserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user, backend="django.contrib.auth.backends.ModelBackend")
            return HttpResponseRedirect('/')
        else:
            return render(request, "registration/create-account.html", {"form": form})

    def get(self, request):
        form = MyUserCreationForm()
        return render(request, "registration/create-account.html", {"form": form})

class SearchForm(forms.Form):
    """Form for taking in search data.
    """
    Make = forms.CharField(required=True, max_length=20,label='Choose a Brand (Ford/Lincoln)')
    ModelName = forms.CharField(required=True, max_length=20,label='Model Name:')
    VehicleType=forms.CharField(required=True,max_length=20,label='Type: (suv, car, crossover, hybrid, truck, commercial-truck)')
 # price_range=forms.IntegerField(required=True)

def search_form(request):
    form=SearchForm()
    return render(request,'searchform.html',context={'form':form})

def recommender(make,model_name,type):
    """Function that contains logic for recommending cars.

    :param make: Make requested by user,
    :param model_name: model name requested by user,
    :param type: Vehicletype requested by user,
    :return: list of 5 cars satisfying requirements.
    """
    recommended_cars = Car.objects.filter(Make__icontains=make, ModelName__icontains=model_name,
                                       VehicleType__icontains=type)[:5]
    return recommended_cars


def search(request):
 if request.method=='POST':
     form=SearchForm(request.POST)
     if form.is_valid():
         val=form.cleaned_data
         make=val['Make']
         modelname=val['ModelName']
         vehicletype=val['VehicleType']
         # price=val['price_range']
         searched_cars = recommender(make,modelname,vehicletype)
         car_data = obj_img_list(searched_cars)
         return render(request,'searchresult.html',context={'car_data': car_data })

