from . import views
from django.contrib import admin
from django.conf.urls import url
from django.contrib.auth import views as auth_views

urlpatterns = [
    url(r'^$', views.homepage, name="home"),
    url(r'^home$',views.homepage, name="home"),
    url(r'^imgview$',views.imgview, name="imgview"),
    url(r'^login/$', auth_views.login, name='login'),
    url(r'^logout/$', auth_views.logout, name='logout'),
    url(r'^create-profile/$', views.CreateProfileView.as_view(), name="create_profile"),
    url(r'^favorites/$',views.favorites, name="favorites"),
    url(r'^searchresult/$', views.search, name="search"),
    url(r'^searchform/$', views.search_form, name="searchform"),
    #url(r'^api/C(?<car_id>\d+)',views.favorites,name="favorites")

]
