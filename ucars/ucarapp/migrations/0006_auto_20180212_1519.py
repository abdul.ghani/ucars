# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2018-02-12 15:19
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ucarapp', '0005_auto_20180212_1459'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fuelefficiency',
            name='maxFuel',
            field=models.PositiveSmallIntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='fuelefficiency',
            name='minFuel',
            field=models.PositiveSmallIntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='seatingcapacity',
            name='maxSeating',
            field=models.PositiveSmallIntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='seatingcapacity',
            name='minSeating',
            field=models.PositiveSmallIntegerField(blank=True, null=True),
        ),
    ]
