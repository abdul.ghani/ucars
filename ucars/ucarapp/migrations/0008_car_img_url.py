# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2018-02-13 11:39
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ucarapp', '0007_auto_20180213_0543'),
    ]

    operations = [
        migrations.AddField(
            model_name='car',
            name='Img_url',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
