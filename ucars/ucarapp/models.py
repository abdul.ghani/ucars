from django.db import models
from django.contrib.auth.models import User

class Car(models.Model):
    """Car model: has attributes make,model,type and year.
        Provides foreign key for all other models.
    """
    TYPE_CHOICES=(
        ("suv", "suv"),
        ("car", "car"),
        ("crossover", "crossover"),
        ("hybrid", "hybrid"),
        ("commercial-truck", "commercial-truck"),
        ("truck", "truck")
    )
    MAKE_CHOICES=(
        ('Ford', 'Ford'),
        ('Lincoln', 'Lincoln')
    )
    Make = models.CharField(max_length=8, choices=MAKE_CHOICES)
    ModelName = models.CharField(max_length=50)
    VehicleType = models.CharField(max_length=30,choices=TYPE_CHOICES)
    Year = models.SmallIntegerField(null =True, blank= True)
    Href = models.CharField(max_length=100, blank= True)

    def save(self, *args, **kwargs):
        super(Car, self).save(*args, **kwargs)



class Pricing(models.Model):
    """Pricing models: Has attributes for the three prices;
        and a range attribute for High price and low price;
        foreign key reference with car.
    """
    RANGES = (
        ("High", "High"),
        ("Low", "Low")
    )
    car = models.ForeignKey(Car)
    Range = models.CharField(max_length=4,choices=RANGES)
    BaseXPlan = models.CharField(max_length=13)
    BaseAZPlan = models.CharField(max_length=12)
    BaseMSRP = models.CharField(max_length=12)

    def save(self, *args, **kwargs):
        super(Pricing, self).save(*args, **kwargs)

class Favorites(models.Model):
    class meta:
        unique_together = ('User_fav','Car_fav')
    User_fav = models.ForeignKey(User)
    Car_fav = models.ForeignKey(Car)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    def save(self,*args, **kwargs):
        super(Favorites,self).save(*args, **kwargs)

class Attribute(models.Model):
    CarObj = models.ForeignKey(Car)
    Key = models.CharField(max_length=30,blank=True, null=True)
    Value = models.CharField(max_length=120,blank=True,null=True)
    def save(self, *args, **kwargs):
        super(Attribute,self).save(*args, **kwargs)








